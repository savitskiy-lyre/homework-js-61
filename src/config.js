export const BASE_URL = 'https://restcountries.com/';
export const COUNTRIES_URL = 'v3.1/all';
export const COUNTRY_3CODE_URL = 'v3.1/alpha/';